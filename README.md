# Santos et al (2021) - Examining signatures of natural selection in antifungal resistance genes across _Aspergillus_ fungi

This repository hosts data used for the evolutionary analyses of genes involved in antifungal resistance described in the Frontiers in Fungal Biology manuscript:

 * "Examining signatures of natural selection in antifungal resistance genes across _Aspergillus_ fungi", Frontiers in Fungal Biology ([DOI: 10.3389/ffunb.2021.723051](https://www.frontiersin.org/articles/10.3389/ffunb.2021.723051/full))

**Folders**
 * _**phylogeneticAnalysis**_: sequences of four phylogenetic markers used to reconstruct the phylogeny
 * _**Orthogroups**_: orthogroups generated with OrthoFinder2 used to recover markers used in phylogeny and genes involved in antifungal resistance across _Fumigati_
 * _**dnds_Alignments**_: codon alignment generated as described in the manuscript, used to run codeml (positive selection analyses)

 **Analyses**
 * A detailed description of the dn/ds analyses is provided below ("Description of dN/dS analyses").

**Contact information**: Renato A. Corrêa dos Santos
  * e-mail: [renatoacsantos[@]gmail.com](mailto:renatoacsantos@gmail.com)
  * [Google Scholar](https://scholar.google.com.br/citations?user=22CKgnIAAAAJ)
  * Twitter: CorreaSantosRA
  * [LinkedIn profile](https://www.linkedin.com/in/renato-augusto-corr%C3%AAa-dos-santos-263202132/)

## Description of dN/dS analyses

Genes related to antifungal resistance were used in analyses of positive selection.
Single-copy orthogroups (recovered from OrthoFinder2 results, folder: `Orthogroups/`) were used.
As input for analyses, the species tree (folder: `phylogeneticAnalysis/`) was used. In cases where an ortholog was missing in one or in a few strains, subtrees from the species tree were obtained using [TreeHouse](https://doi.org/10.1186/s13104-019-4577-5) (nice software, check this out!).

**Steps**:
 * Sequence alignment (amino acid and codon-aware CDS alignment)
 * TreeHouse (pruning the species tree)
 * codeml (PAML v.4.9i)
 * Likelihood Ratio Test (LRT)
 * Bayes Empirical Bayes (BEB)

### Sequence alignment

#### Sequence sources

For each single-copy ortholog, the amino acid and coding sequences were recovered.
In cases the coding sequence was available at NCBI databases, the `efetch 14.3` software was used for recovery. In cases gene prediction was not available, we used Augustus locally.

#### Aligning amino acid sequences

Amino acid sequences were aligned using MAFFT v7.397. An example is provided below for cyp51A:

`mafft --auto cyp51A_aa.fasta > cyp51A_aa.aligned.fasta`

#### Aligning coding sequences (CDS)

Using both the CDS and the amino acid alignment, codon-aware alignment was generated for the nucleotide sequences, using the online version of [pal2nal](http://www.bork.embl.de/pal2nal/).

#### Recovering a pruned tree

The species tree was provided as input to [TreeHouse](https://github.com/JLSteenwyk/treehouse).
In addition to the tree, a list of strain identifiers corresponding to the subset that have the `cyp51A` gene was provided to TreeHouse.

#### Recovering the log likelihood

codeml (as implemented in PAML 4.9i) was used to calculate the log likelihood (lnL), or "the probability of data fitting the model" ([Jeffares et al (2015)](https://link.springer.com/protocol/10.1007/978-1-4939-1438-8_4)). As input in these analyses, it was necessary to provide a tree (output from TreeHouse) and the codon alignment, and indicate the models. Four site models were used:

| Model | Description                                  |
|-------|----------------------------------------------|
| M1a   | two classes of sites (ω < 1; ω = 1)          |
| M2a   | three classes of sites (ω < 1; ω = 1; ω > 1) |
| M7    | ten classes with flexible normalized non-synonymous ratio distribution |
| M8    | ten classes with flexible normalized non-synonymous ratio distribution, plus one extra class with ω > 1 |

#### Calculating the LTR

LRT is calculated as twice the difference of the log-likelihood between the null model and alternative model.

Degrees of freedom (k) for the test is calculated as k = p 1 − p 0, where p 1 is the number of parameters estimated in the alternative model, and p 0 is the number of parameters in the null model.

We Assessed significance of the LRT with χ2 value (conservative test for this purpose, as described in [Jeffares et al](https://link.springer.com/protocol/10.1007/978-1-4939-1438-8_4)).

 * M1 (null model) vs. M2 (alternative)
 * M7 (null model) vs. M8 (alternative)

#### Recovering sites under positive selection

For genes whose sites under positive were identified, a script in this repository, [checkPositionsAfterGaps.py](https://gitlab.com/SantosRAC/Santosetal2021_evolutionGenesAntifungalsFumigati/-/blob/master/scripts/checkPositionsAfterGaps.py) to recover the correct position in the reference _A. fumigatus_ strain, the _A. fumigatus_ A1163.



