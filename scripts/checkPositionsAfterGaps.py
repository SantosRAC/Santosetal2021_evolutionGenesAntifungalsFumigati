#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda3/bin/python3

from Bio import AlignIO
from Bio import SeqIO
import argparse
import sys

version=sys.argv[0] + ' ' + "0.1"
parser = argparse.ArgumentParser(description='Recalculates position of sites under positive selection (codeml in PAML v.4.9i), accounting for alignment gaps', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--input_alignment', dest='inALIGNMENT', metavar='ALIGNMENT file', type=str, help='Alignment file', required=True)
parser.add_argument('--format', dest='fileFormat', metavar='File format (fasta, phylip, or phylip-sequential)', type=str, help='File format of the alignment file', required=True)
parser.add_argument('--ref_sequence_id', dest='refIdentifier', metavar='Identifier of the reference sequence', type=str, help='Sequence identifier of the reference (usually the first)', required=True)
parser.add_argument('--ref_sequence', dest='refSequence', metavar='Reference sequence', type=str, help='Sequence of reference (FASTA)', required=True)
parser.add_argument('-p','--pos_codeml', dest='posCodeml', metavar='Amino acid position in codeml', type=int, help='Position for a given site of the reference in codeml', required=True)

args = parser.parse_args()
alignFile = args.inALIGNMENT
alignFileFormat = args.fileFormat
refID = args.refIdentifier
refSeq = args.refSequence
position_codeml = args.posCodeml # position in reference after removing gaps in any of the sequences

listAlignIndexGap=[]

alignFileOBJ = AlignIO.read(open(alignFile), alignFileFormat)

position_codeml_codon = position_codeml * 3 # The codon position after multiplying "position_codeml" position by 3

# Iterates over sequences in alignment given as input
# Records each position/homologous column that has a gap in at least one of the homologous sequences
for record in alignFileOBJ:
 for index, letter in enumerate(record):
  if letter == "-":
   if not (index in listAlignIndexGap):
    listAlignIndexGap=listAlignIndexGap+[index]

# (non-index) position in reference sequence
countgapposref = 0
countgapposany = 0
reportedcodemlcodonpositions=[]

# Read each sequence in the alignment file
for record in alignFileOBJ:
 # Only read the reference sequence ("refID")
 if record.id == refID:
  posref = 0
  posalignment = 1
  for index, letter in enumerate(record):
   # Record aa position in reference sequence
   if letter == "-":
    countgapposref+=1
   else:
    posref+=1
   # Records number of gaps observed so for in homologous position, based on other homologous sequences as well.
   if index in listAlignIndexGap:
    countgapposany+=1
   if (posalignment - countgapposany) == position_codeml_codon:
    if not (position_codeml_codon in reportedcodemlcodonpositions):
     reportedcodemlcodonpositions=reportedcodemlcodonpositions+[position_codeml_codon]
     actual_aa=posref/3
     num = len([1 for line in open(refSeq) if line.startswith(">")])
     if num == 1:
      for record in SeqIO.parse(refSeq, "fasta"):
       index_actual_aa=int(actual_aa-1)
       #print("Position_codeml_codon	Nucleotide_aligment_position	Position_on_reference_sequence_nucleotide_alignment	Number_of_gaps_any_position	Number_of_gaps_reference_seq	Position_on_reference_sequence_aa	AA_reference")
       print(position_codeml_codon,posalignment,posref,countgapposany,countgapposref,actual_aa,record.seq[index_actual_aa])
   posalignment+=1
